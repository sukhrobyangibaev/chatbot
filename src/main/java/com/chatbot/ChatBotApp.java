package com.chatbot;

import akka.actor.typed.ActorSystem;

public class ChatBotApp {
    public static void main(String[] args){
        ActorSystem<ChatBotActor.Command> chatBotSystem = ActorSystem.create(ChatBotActor.create(), "ChatBotSystem");
        chatBotSystem.tell(new ChatBotActor.GetQuestion());
    }
}
