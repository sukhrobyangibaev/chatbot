package com.chatbot;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ChatBotActor extends AbstractBehavior<ChatBotActor.Command> {

    interface Command{}

    public static class MakeRequest implements Command{
        public final String question;

        public MakeRequest(String question) {
            this.question = question;
        }
    }

    public static class GetQuestion implements Command{
        public GetQuestion() {}
    }

    public static class AskUser implements Command{
        public AskUser(){}
    }

    public static Behavior<Command> create(){
        return Behaviors.setup(ChatBotActor::new);
    }

    public ChatBotActor(ActorContext<Command> context) {
        super(context);
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(MakeRequest.class, this::onMakeRequest)
                .onMessage(GetQuestion.class, this::onGetQuestion)
                .onMessage(AskUser.class, this::onAskUser)
                .build();
    }

    private Behavior<Command> onMakeRequest(MakeRequest mr) throws MalformedURLException {
        String dataIn = "{ \"context\": [ \"" + mr.question + "\" ]}";
        String stringUrl = "https://odqa.demos.ivoice.online/model";
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            connection.setConnectTimeout(3000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type","application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            OutputStream os = connection.getOutputStream();
            os.write(dataIn.getBytes());
            os.close();
            InputStream is = new BufferedInputStream(connection.getInputStream());
            String answer = IOUtils.toString(is);

            System.out.println(answer.substring(answer.indexOf("\"") + 1, answer.lastIndexOf("\"")));

            is.close();
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getContext().getSelf().tell(new AskUser());
        return this;
    }

    private Behavior<Command> onGetQuestion(GetQuestion gq) throws IOException {
        System.out.println("please, ask your question");
        String question = getString();
        getContext().getSelf().tell(new MakeRequest(question));
        return this;
    }

    private Behavior<Command> onAskUser(AskUser au) throws IOException {
        System.out.print("Ask next question? Y/n [Y] ");
        char ask = getChar();
        if(ask == 'Y' || ask == 'y')
            getContext().getSelf().tell(new GetQuestion());
        else if(ask == 'N' || ask == 'n')
            getContext().getSystem().terminate();
        else{
            System.out.println("Invalid entry");
            getContext().getSelf().tell(new AskUser());
        }

        return this;
    }

    public static String getString() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        return br.readLine();
    }

    public static char getChar() throws IOException {
        String s = getString();
        if(s.length() == 0)
            return 'Y';
        return s.charAt(0);
    }
}
