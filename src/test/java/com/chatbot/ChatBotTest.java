package com.chatbot;

import akka.actor.testkit.typed.javadsl.TestKitJunitResource;
import akka.actor.testkit.typed.javadsl.TestProbe;
import akka.actor.typed.ActorRef;
import org.junit.ClassRule;
import org.junit.Test;

public class ChatBotTest {

    @ClassRule
    public static final TestKitJunitResource testKit = new TestKitJunitResource();

    @Test
    public void testAskUser(){
        TestProbe<ChatBotActor.AskUser> testProbe = testKit.createTestProbe();
        ActorRef<ChatBotActor.Command> test = testKit.spawn(ChatBotActor.create(),"s");
    }

    @Test
    public void testGetQuestion(){
        TestProbe<ChatBotActor.GetQuestion> testProbe = testKit.createTestProbe();
    }

    @Test
    public void testMakeRequest(){
        TestProbe<ChatBotActor.MakeRequest> testProbe = testKit.createTestProbe();
    }
}
